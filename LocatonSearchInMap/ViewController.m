//
//  ViewController.m
//  LocatonSearchInMap
//
//  Created by click labs 115 on 10/14/15.
//  Copyright (c) 2015 cli. All rights reserved.
//

#import "ViewController.h"

@interface ViewController (){
    CLLocationManager *manager;
    CLLocation *currentLocation;
    NSString *city;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self getaddress ];
    
    manager = [[CLLocationManager alloc]init];
    manager.delegate = self;
    manager.desiredAccuracy = kCLLocationAccuracyBest;

    [manager startUpdatingLocation];

    _searchTable.hidden = YES;
//    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
//                                   initWithTarget:self
//                                   action:@selector(dismissTable)];
//    
    //[_googleMap addGestureRecognizer:tap];
   // CLLocationCoordinate2D position = CLLocationCoordinate2DMake(30.7500, 76.7800);
   // GMSMarker *marker = [GMSMarker markerWithPosition:position];
   // marker.title = @"Welcome Chandigarh";
    //marker.map = _googleMap;
    // Add an annotation
   // MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, 30.7500, 76.7800);
 self.appleMap.delegate = self;
    
    [manager requestWhenInUseAuthorization];

    // Do any additional setup after loading the view, typically from a nib.
}
- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
    
}
- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 5;
}
-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"reuse"];
    
    return cell;
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
    
     _searchTable.hidden = NO;
    return YES;
}
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
//    UIAlertView *errorAlert = [[UIAlertView alloc]
//                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{

    NSLog(@"didUpdateToLocation: %@", newLocation);
    currentLocation = newLocation;

    if (currentLocation != nil) {
        MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
        point.coordinate =  currentLocation.coordinate;
        point.title = [NSString stringWithFormat:@"%@",city];
        point.subtitle = @"I'm here!!!";
       
           // MKMapCamera *newCamera = [[_appleMap camera] copy];
        MKMapCamera *newCamera = [MKMapCamera cameraLookingAtCenterCoordinate:currentLocation.coordinate
                                                         fromEyeCoordinate:currentLocation.coordinate
                                                               eyeAltitude:50];
            [newCamera setHeading:100.0]; // or newCamera.heading + 90.0 % 360.0
            [newCamera setPitch:45.0];
            //[newCamera setHeading:90.0];
           // [newCamera setAltitude:500.0];
            [_appleMap setCamera:newCamera animated:YES];
        
        [self.appleMap addAnnotation:point];
        
        
        CLLocationCoordinate2D position = currentLocation.coordinate;
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:currentLocation.coordinate.latitude
                                                                longitude:currentLocation.coordinate.longitude
                                                                     zoom:10.0];
        [_googleMap animateToCameraPosition:camera];
        GMSMarker *marker = [GMSMarker markerWithPosition:position];
        marker.title = [NSString stringWithFormat:@"%@",city];
    
        marker.map = _googleMap;
        [self getaddress];
        

    }
    

}
    -(void) getaddress  {
        CLGeocoder *geocoder = [[CLGeocoder alloc] init];
        
        CLLocation *newLocation = [[CLLocation alloc]initWithLatitude:currentLocation.coordinate.latitude
                                                            longitude:currentLocation.coordinate.longitude];
        
        [geocoder reverseGeocodeLocation:newLocation
                       completionHandler:^(NSArray *placemarks, NSError *error) {
                           
                           if (error) {
                               NSLog(@"Geocode failed with error: %@", error);
                               return;
                           }
                           
                           if (placemarks && placemarks.count > 0)
                           {
                               CLPlacemark *placemark = placemarks[0];
                               
                               NSDictionary *addressDictionary =
                               placemark.addressDictionary;
                               
                               NSLog(@"%@ ", addressDictionary);
                               city = addressDictionary[@"City"];
                               NSLog(@"%@",city);
                                //point.title = [NSString stringWithFormat:@"%@",city];
//                               NSString *address = [addressDictionary
//                                                    objectForKey:(NSString *)kABPersonAddressStreetKey];
//                               NSString *city = [addressDictionary
//                                                 objectForKey:(NSString *)kABPersonAddressCityKey];
//                               NSString *state = [addressDictionary
//                                                  objectForKey:(NSString *)kABPersonAddressStateKey];
//                               NSString *zip = [addressDictionary 
//                                                objectForKey:(NSString *)kABPersonAddressZIPKey];
//                               
//                               
//                               NSLog(@"%@ %@ %@ %@", address,city, state, zip);
                           }
                           
                 
                       }];
        
    }




 
/*
- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, 30.7500, 76.7800);
    [self.appleMap setRegion:[self.appleMap regionThatFits:region] animated:YES];
}


- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, 800, 800);
    [self.appleMap setRegion:[self.appleMap regionThatFits:region] animated:YES];
    
    // Add an annotation
    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
    point.coordinate = userLocation.coordinate;
    point.title = @"Where am I?";
    point.subtitle = @"I'm here!!!";
    
    [self.appleMap addAnnotation:point];
}*/
/*
- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar{
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    return  YES;
}*/

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
